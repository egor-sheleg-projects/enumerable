﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;

namespace EnumerableTask
{
    public class EnumerableManipulation
    {
        /// <summary> Transforms all strings to upper case.</summary>
        /// <param name="data">Source string sequence.</param>
        /// <returns>
        ///   Returns sequence of source strings in uppercase.
        /// </returns>
        /// <example>
        ///    {"a", "b", "c"} => { "A", "B", "C" }
        ///    { "A", "B", "C" } => { "A", "B", "C" }
        ///    { "a", "A", "", null } => { "A", "A", "", null }.
        /// </example>
        public IEnumerable<string> GetUppercaseStrings(IEnumerable<string>? data)
        {
            if (data is null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            IList<string> uppercaseStrings = new List<string>();
            foreach (string i in data)
            {
                if (string.IsNullOrEmpty(i))
                {
                    uppercaseStrings.Add(i);
                }
                else
                {
                    uppercaseStrings.Add(i.ToUpperInvariant());
                }
            }

            return uppercaseStrings;
        }

        /// <summary> Transforms an each string from sequence to its length.</summary>
        /// <param name="data">Source strings sequence.</param>
        /// <returns>
        ///   Returns sequence of strings length.
        /// </returns>
        /// <example>
        ///   { } => { }
        ///   {"a", "aa", "aaa" } => { 1, 2, 3 }
        ///   {"aa", "bb", "cc", "", "  ", null } => { 2, 2, 2, 0, 2, 0 }.
        /// </example>
        public IEnumerable<int> GetStringsLength(IEnumerable<string>? data)
        {
            if (data is null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            IList<int> stringsLength = new List<int>();
            foreach (string i in data)
            {
                if (string.IsNullOrEmpty(i))
                {
                    stringsLength.Add(0);
                }
                else
                {
                    stringsLength.Add(i.Length);
                }
            }

            return stringsLength;
        }

        /// <summary>Transforms integer sequence to its square sequence, f(x) = x * x. </summary>
        /// <param name="data">Source int sequence.</param>
        /// <returns>
        ///   Returns sequence of squared items.
        /// </returns>
        /// <example>
        ///   { } => { }
        ///   { 1, 2, 3, 4, 5 } => { 1, 4, 9, 16, 25 }
        ///   { -1, -2, -3, -4, -5 } => { 1, 4, 9, 16, 25 }.
        /// </example>
        public IEnumerable<long> GetSquareSequence(IEnumerable<int>? data)
        {
            if (data is null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            IList<long> squareSequence = new List<long>();
            foreach (long i in data)
            {
                squareSequence.Add(i * i);
            }

            return squareSequence;
        }

        /// <summary> Filters a string sequence by a prefix value (case insensitive).</summary>
        /// <param name="data">Source string sequence.</param>
        /// <param name="prefix">Prefix value to filter.</param>
        /// <returns>
        ///  Returns items from data that started with required prefix (case insensitive).
        /// </returns>
        /// <exception cref="ArgumentNullException">Thrown when prefix is null.</exception>
        /// <example>
        ///  { "aaa", "bbbb", "ccc", null }, prefix = "b"  =>  { "bbbb" }
        ///  { "aaa", "bbbb", "ccc", null }, prefix = "B"  =>  { "bbbb" }
        ///  { "a","b","c" }, prefix = "D"  => { }
        ///  { "a","b","c" }, prefix = ""   => { "a","b","c" }
        ///  { "a","b","c", null }, prefix = ""   => { "a","b","c" }
        ///  { "a","b","c" }, prefix = null => ArgumentNullException.
        /// </example>
        public IEnumerable<string> GetPrefixItems(IEnumerable<string>? data, string prefix)
        {
            if (data is null)
            {
                throw new ArgumentNullException(nameof(data));
            }
            else if (prefix is null)
            {
                throw new ArgumentNullException(nameof(prefix));
            }

            IList<string> prefixItems = new List<string>();
            foreach (string i in data)
            {
                if (i is not null && i.StartsWith(prefix.ToLowerInvariant(), StringComparison.InvariantCultureIgnoreCase))
                {
                    prefixItems.Add(i);
                }
            }

            return prefixItems;
        }

        /// <summary> Finds the 3 largest numbers from a sequence.</summary>
        /// <param name="data">Source sequence.</param>
        /// <returns>
        ///   Returns the 3 largest numbers from a sequence.
        /// </returns>
        /// <example>
        ///   { } => { }
        ///   { 1, 2 } => { 2, 1 }
        ///   { 1, 2, 3 } => { 3, 2, 1 }
        ///   { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 } => { 10, 9, 8 }
        ///   { 10, 10, 10, 10 } => { 10, 10, 10 }.
        /// </example>
        public IEnumerable<int> Get3LargestItems(IEnumerable<int>? data)
        {
            if (data is null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            int max = 0;
            IList<int> largestItems = new List<int>();
            foreach (int i in data)
            {
                if (i >= max)
                {
                    if (largestItems.Count < 3)
                    {
                        largestItems.Insert(0, i);
                        max = largestItems.Min();
                    }
                    else if (largestItems.Count == 3)
                    {
                        if (i >= largestItems[0])
                        {
                            largestItems.RemoveAt(2);
                            largestItems.Insert(0, i);
                        }
                        else if (i >= largestItems[1])
                        {
                            largestItems.RemoveAt(2);
                            largestItems.Insert(2, largestItems[1]);
                            largestItems.Insert(1, i);
                        }
                        else
                        {
                            largestItems.RemoveAt(2);
                            largestItems.Insert(2, i);
                        }

                        max = largestItems.Min();
                    }
                }
            }

            return largestItems;
        }

        /// <summary> Calculates sum of all integers from object array.</summary>
        /// <param name="data">Source array.</param>
        /// <returns>
        ///    Returns the sum of all integers from object array.
        /// </returns>
        /// <example>
        ///    { 1, true, "a", "b", false, 1 } => 2
        ///    { true, false } => 0
        ///    { 10, "ten", 10 } => 20
        ///    { } => 0.
        /// </example>
        public int GetSumOfAllIntegers(object[] data)
        {
            if (data is null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            int sumOfAllIntegers = 0;
            foreach (var i in data)
            {
                if (i != null)
                {
                    Type t = i.GetType();
                    if (t.Equals(typeof(int)))
                    {
                        sumOfAllIntegers += (int)i;
                    }
                }
            }

            return sumOfAllIntegers;
        }
    }
}
